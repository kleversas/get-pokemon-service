
Get pokemon service
Test project to obtain different types of pokemon.


Starting
Clone with https: https://gitlab.com/kleversas/get-pokemon-service.git
Clone with ssh: git@gitlab.com:kleversas/get-pokemon-service.git


Project structure

- common
- config
- controller
- external
- mapper
- models
- service
- utils

Common: It contains a class of constants used for the development of the project, default values, error messages and cache names.
Config: Contains swagger, restemplate, and cache instances settings
Controller: Exposes two api that receive the id of the pokemon and perform the queries
External: Service that makes the call to the different POKEAPI APIs
Mapper: It is responsible for transforming the information consulted into useful information for the user
Models: Here are the different entities and data that are used to build the requests to the POKEAPI and the responses of the services
Service: Here is the main service of the application that is in charge of managing the cache and making the different calls to obtain the information of the Pokemons
Utils: Contains a utility to control calls to the POKEAPI client and handle exceptions

Tests Cases 
Integration tests were used to cover all the flows within the process, calls, exceptions and data return were simulated through mocks.

Deploy
The deployment was carried out with Heroku following the following steps:

- Create account in Heroku
- Login through the Heroku CLI
- Create an application in Heroku using the command "heroku create"
- Connect git with the source repository of the application in Heroku
- Create a System.properties file with the configuration of the Java version
- Perform push to the main branch in Heroku using the command "git push heroku main"

Documentation swagger:
https://serene-waters-76318.herokuapp.com/swagger-ui.html#/

Postman Collection
https://www.getpostman.com/collections/8a4637a67a374ef1e590

Front de Prueba
https://app-pokemon-modyo.herokuapp.com/




Construido con 
Java 11
Maven Apache Maven 3.6.3
Spring boot

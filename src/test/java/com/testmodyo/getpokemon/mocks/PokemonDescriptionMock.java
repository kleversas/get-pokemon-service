package com.testmodyo.getpokemon.mocks;

import java.util.LinkedList;
import java.util.List;

import com.testmodyo.getpokemon.EntityMockeable;
import com.testmodyo.getpokemon.models.domain.Description;
import com.testmodyo.getpokemon.models.domain.PokemonDescription;

public class PokemonDescriptionMock implements EntityMockeable<PokemonDescription> {

	@Override
	public PokemonDescription generate() {
		PokemonDescription pokemonDescription = new PokemonDescription();
		List<Description> descriptions = new LinkedList<>();
		Description description = new Description();
		description.setDescription("description");
		descriptions.add(description);
		descriptions.add(description);
		descriptions.add(description);
		pokemonDescription.setDescriptions(descriptions);
		return pokemonDescription;
	}

}

package com.testmodyo.getpokemon.mocks;

import java.util.LinkedList;
import java.util.List;

import com.testmodyo.getpokemon.EntityMockeable;
import com.testmodyo.getpokemon.models.domain.Ability;
import com.testmodyo.getpokemon.models.domain.AbilityDetail;
import com.testmodyo.getpokemon.models.domain.Pokemon;
import com.testmodyo.getpokemon.models.domain.Specie;
import com.testmodyo.getpokemon.models.domain.Sprites;
import com.testmodyo.getpokemon.models.domain.Type;
import com.testmodyo.getpokemon.models.domain.TypeDetail;

public class PokemonMock implements EntityMockeable<Pokemon> {

	@Override
	public Pokemon generate() {
		Pokemon pokemon = new Pokemon();
		AbilityDetail abilityDetail = new AbilityDetail();
		Ability ability = new Ability();
		ability.setName("ability1");
		abilityDetail.setAbility(ability);
		TypeDetail typeDetail = new TypeDetail();
		Type type = new Type();
		type.setName("type1");
		typeDetail.setType(type);
		List<AbilityDetail> abilities = new LinkedList<>();
		List<TypeDetail> types = new LinkedList<>();
		abilities.add(abilityDetail);
		types.add(typeDetail);
		pokemon.setAbilities(abilities);
		pokemon.setTypes(types);
		pokemon.setId(1);
		Sprites sprite = new Sprites();
		sprite.setBackDefault("default");
		pokemon.setSprites(sprite);
		Specie specie = new Specie();
		specie.setUrl("http://local/test/tes2/api/1");
		pokemon.setSpecies(specie);
		return pokemon;
	}

}

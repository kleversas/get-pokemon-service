package com.testmodyo.getpokemon.mocks;

import com.testmodyo.getpokemon.EntityMockeable;
import com.testmodyo.getpokemon.models.domain.EvolutionSpecie;
import com.testmodyo.getpokemon.models.domain.PokemonSpecie;

public class PokemonSpecieMock implements EntityMockeable<PokemonSpecie> {

	@Override
	public PokemonSpecie generate() {
		PokemonSpecie pokemonSpecie = new PokemonSpecie();
		EvolutionSpecie evolutionChain = new EvolutionSpecie();
		evolutionChain.setUrl("http://local/test/tes2/api/1");
		pokemonSpecie.setEvolutionChain(evolutionChain);
		return pokemonSpecie;
	}

}

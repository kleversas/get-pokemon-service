package com.testmodyo.getpokemon.mocks;

import java.util.LinkedList;
import java.util.List;

import com.testmodyo.getpokemon.EntityMockeable;
import com.testmodyo.getpokemon.models.domain.Chain;
import com.testmodyo.getpokemon.models.domain.EvolutionChains;
import com.testmodyo.getpokemon.models.domain.Specie;

public class EvolutionsMock implements EntityMockeable<EvolutionChains> {

	@Override
	public EvolutionChains generate() {
		EvolutionChains evolutionChains = new EvolutionChains();
		Chain chain = new Chain();
		Specie specie = new Specie();
		specie.setUrl("http://local/test/tes2/api/2");
		chain.setSpecies(specie);
		List<Chain> evolvesTo = new LinkedList<>();
		evolvesTo.add(chain);
		chain.setEvolvesTo(evolvesTo);
		evolutionChains.setChain(chain);
		return evolutionChains;
	}

}

package com.testmodyo.getpokemon.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.testmodyo.getpokemon.GetPokemonApplication;
import com.testmodyo.getpokemon.common.CommonConstants;
import com.testmodyo.getpokemon.external.ICallPokeApiService;
import com.testmodyo.getpokemon.mapper.IMapperService;
import com.testmodyo.getpokemon.mocks.EvolutionsMock;
import com.testmodyo.getpokemon.mocks.PokemonDescriptionMock;
import com.testmodyo.getpokemon.mocks.PokemonMock;
import com.testmodyo.getpokemon.mocks.PokemonSpecieMock;
import com.testmodyo.getpokemon.models.domain.EvolutionChains;
import com.testmodyo.getpokemon.models.domain.Pokemon;
import com.testmodyo.getpokemon.models.domain.PokemonDescription;
import com.testmodyo.getpokemon.models.domain.PokemonSpecie;
import com.testmodyo.getpokemon.models.dto.ResponseDto;
import com.testmodyo.getpokemon.service.IPokemonService;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = GetPokemonApplication.class)
@WebMvcTest({ PokemonController.class, IPokemonService.class, IMapperService.class, ICallPokeApiService.class })
class PokemonControllerIntegrationTest {

	public static final String DEFAULT_API_GET_POKEMON = "/api/v1/pokemon/get-pokemon/";
	public static final String DEFAULT_API_GET_POKEMON_DETAIL = "/api/v1/pokemon/get-pokemon-detail/";

	@Autowired
	private MockMvc mvc;

	@Autowired
	ObjectMapper mapper;

	@MockBean
	private RestTemplate restTemplate;

	private PokemonMock pokemonMock = new PokemonMock();

	private PokemonDescriptionMock pokemonDescriptionMock = new PokemonDescriptionMock();

	private PokemonSpecieMock pokemonSpecieMock = new PokemonSpecieMock();

	private EvolutionsMock evolutionsMock = new EvolutionsMock();

	private MvcResult getResult(String number, String api) throws Exception {
		return this.mvc.perform(
				get(api.concat(number)).accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON))
				.andReturn();
	}

	@Test
	void getPokemonsSuccessful() throws Exception {

		when(restTemplate.exchange(anyString(), any(HttpMethod.class), Mockito.any(), eq(Pokemon.class)))
				.thenReturn(new ResponseEntity<Pokemon>(pokemonMock.generate(), HttpStatus.OK));
		MvcResult result = getResult("1", DEFAULT_API_GET_POKEMON);
		ResponseDto response = mapper.readValue(result.getResponse().getContentAsString(), ResponseDto.class);

		Assertions.assertEquals(HttpStatus.OK.value(), response.getStatus());
		Assertions.assertEquals(CommonConstants.SUCCESSFUL_OPERATION, response.getCode());

	}

	@Test
	void getPokemonNotFound() throws Exception {
		HttpClientErrorException httpClientErrorException = new HttpClientErrorException(HttpStatus.NOT_FOUND);
		when(restTemplate.exchange(anyString(), any(HttpMethod.class), Mockito.any(), eq(Pokemon.class)))
				.thenThrow(httpClientErrorException);
		MvcResult result = getResult("2", DEFAULT_API_GET_POKEMON);
		ResponseDto response = mapper.readValue(result.getResponse().getContentAsString(), ResponseDto.class);

		Assertions.assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatus());
		Assertions.assertEquals(CommonConstants.CLIENT_ERROR_GET_POKEMON, response.getCode());

	}

	@Test
	void getPokemonBadRequest() throws Exception {

		MvcResult result = getResult("S", DEFAULT_API_GET_POKEMON);
		ResponseDto response = mapper.readValue(result.getResponse().getContentAsString(), ResponseDto.class);

		Assertions.assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());
		Assertions.assertEquals(CommonConstants.INVALID_REQUEST, response.getCode());

	}

	@Test
	void getPokemonDetailSuccessful() throws Exception {

		when(restTemplate.exchange(anyString(), any(HttpMethod.class), Mockito.any(), eq(Pokemon.class)))
				.thenReturn(new ResponseEntity<Pokemon>(pokemonMock.generate(), HttpStatus.OK));
		when(restTemplate.exchange(anyString(), any(HttpMethod.class), Mockito.any(), eq(PokemonDescription.class)))
				.thenReturn(new ResponseEntity<PokemonDescription>(pokemonDescriptionMock.generate(), HttpStatus.OK));
		when(restTemplate.exchange(anyString(), any(HttpMethod.class), Mockito.any(), eq(PokemonSpecie.class)))
				.thenReturn(new ResponseEntity<PokemonSpecie>(pokemonSpecieMock.generate(), HttpStatus.OK));
		when(restTemplate.exchange(anyString(), any(HttpMethod.class), Mockito.any(), eq(EvolutionChains.class)))
				.thenReturn(new ResponseEntity<EvolutionChains>(evolutionsMock.generate(), HttpStatus.OK));

		MvcResult result = getResult("1", DEFAULT_API_GET_POKEMON_DETAIL);

		ResponseDto response = mapper.readValue(result.getResponse().getContentAsString(), ResponseDto.class);

		Assertions.assertEquals(HttpStatus.OK.value(), response.getStatus());
		Assertions.assertEquals(CommonConstants.SUCCESSFUL_OPERATION, response.getCode());

	}

	@Test
	void getPokemonDetailNotFound() throws Exception {
		HttpClientErrorException httpClientErrorException = new HttpClientErrorException(HttpStatus.NOT_FOUND);
		when(restTemplate.exchange(anyString(), any(HttpMethod.class), Mockito.any(), eq(Pokemon.class)))
				.thenThrow(httpClientErrorException);

		MvcResult result = getResult("4", DEFAULT_API_GET_POKEMON_DETAIL);

		ResponseDto response = mapper.readValue(result.getResponse().getContentAsString(), ResponseDto.class);

		Assertions.assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatus());
		Assertions.assertEquals(CommonConstants.CLIENT_ERROR_GET_POKEMON, response.getCode());

	}

	@Test
	void getPokemonDetailDescriptionNotFound() throws Exception {
		HttpClientErrorException httpClientErrorException = new HttpClientErrorException(HttpStatus.NOT_FOUND);
		when(restTemplate.exchange(anyString(), any(HttpMethod.class), Mockito.any(), eq(Pokemon.class)))
				.thenReturn(new ResponseEntity<Pokemon>(pokemonMock.generate(), HttpStatus.OK));
		when(restTemplate.exchange(anyString(), any(HttpMethod.class), Mockito.any(), eq(PokemonDescription.class)))
				.thenThrow(httpClientErrorException);
		when(restTemplate.exchange(anyString(), any(HttpMethod.class), Mockito.any(), eq(PokemonSpecie.class)))
				.thenReturn(new ResponseEntity<PokemonSpecie>(pokemonSpecieMock.generate(), HttpStatus.OK));
		when(restTemplate.exchange(anyString(), any(HttpMethod.class), Mockito.any(), eq(EvolutionChains.class)))
				.thenReturn(new ResponseEntity<EvolutionChains>(evolutionsMock.generate(), HttpStatus.OK));

		MvcResult result = getResult("8", DEFAULT_API_GET_POKEMON_DETAIL);

		ResponseDto response = mapper.readValue(result.getResponse().getContentAsString(), ResponseDto.class);

		Assertions.assertEquals(HttpStatus.OK.value(), response.getStatus());
		Assertions.assertEquals(CommonConstants.SUCCESSFUL_OPERATION, response.getCode());

	}

	@Test
	void getPokemonDetailSpecieNotFound() throws Exception {
		HttpClientErrorException httpClientErrorException = new HttpClientErrorException(HttpStatus.NOT_FOUND);
		when(restTemplate.exchange(anyString(), any(HttpMethod.class), Mockito.any(), eq(Pokemon.class)))
				.thenReturn(new ResponseEntity<Pokemon>(pokemonMock.generate(), HttpStatus.OK));
		when(restTemplate.exchange(anyString(), any(HttpMethod.class), Mockito.any(), eq(PokemonSpecie.class)))
				.thenThrow(httpClientErrorException);
		when(restTemplate.exchange(anyString(), any(HttpMethod.class), Mockito.any(), eq(PokemonDescription.class)))
				.thenReturn(new ResponseEntity<PokemonDescription>(pokemonDescriptionMock.generate(), HttpStatus.OK));
		MvcResult result = getResult("10", DEFAULT_API_GET_POKEMON_DETAIL);

		ResponseDto response = mapper.readValue(result.getResponse().getContentAsString(), ResponseDto.class);

		Assertions.assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatus());
		Assertions.assertEquals(CommonConstants.CLIENT_ERROR_GET_SPECIES, response.getCode());

	}

	@Test
	void getPokemonDetailEvolutionsNotFound() throws Exception {
		HttpClientErrorException httpClientErrorException = new HttpClientErrorException(HttpStatus.NOT_FOUND);
		when(restTemplate.exchange(anyString(), any(HttpMethod.class), Mockito.any(), eq(Pokemon.class)))
				.thenReturn(new ResponseEntity<Pokemon>(pokemonMock.generate(), HttpStatus.OK));
		when(restTemplate.exchange(anyString(), any(HttpMethod.class), Mockito.any(), eq(PokemonDescription.class)))
				.thenReturn(new ResponseEntity<PokemonDescription>(pokemonDescriptionMock.generate(), HttpStatus.OK));
		when(restTemplate.exchange(anyString(), any(HttpMethod.class), Mockito.any(), eq(PokemonSpecie.class)))
				.thenReturn(new ResponseEntity<PokemonSpecie>(pokemonSpecieMock.generate(), HttpStatus.OK));
		when(restTemplate.exchange(anyString(), any(HttpMethod.class), Mockito.any(), eq(EvolutionChains.class)))
				.thenThrow(httpClientErrorException);

		MvcResult result = getResult("12", DEFAULT_API_GET_POKEMON_DETAIL);

		ResponseDto response = mapper.readValue(result.getResponse().getContentAsString(), ResponseDto.class);

		Assertions.assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatus());
		Assertions.assertEquals(CommonConstants.CLIENT_ERROR_GET_EVOLUTIONS, response.getCode());

	}

	@Test
	void getPokemonDetailReturnException() throws Exception {

		MvcResult result = getResult("6", DEFAULT_API_GET_POKEMON_DETAIL);

		ResponseDto response = mapper.readValue(result.getResponse().getContentAsString(), ResponseDto.class);

		Assertions.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), response.getStatus());
		Assertions.assertEquals(CommonConstants.APPLICATION_ERROR, response.getCode());

	}

}

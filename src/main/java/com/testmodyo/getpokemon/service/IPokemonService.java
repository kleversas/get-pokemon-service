package com.testmodyo.getpokemon.service;

import com.testmodyo.getpokemon.controller.exception.BusinessException;
import com.testmodyo.getpokemon.models.dto.PokemonDetailDto;
import com.testmodyo.getpokemon.models.dto.PokemonDto;

public interface IPokemonService {

	public PokemonDetailDto getPokemonDetail(int id) throws BusinessException;

	public PokemonDto getPokemon(int id) throws BusinessException;

}

package com.testmodyo.getpokemon.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.testmodyo.getpokemon.common.CommonConstants;
import com.testmodyo.getpokemon.controller.exception.BusinessException;
import com.testmodyo.getpokemon.external.ICallPokeApiService;
import com.testmodyo.getpokemon.mapper.IMapperService;
import com.testmodyo.getpokemon.models.domain.Chain;
import com.testmodyo.getpokemon.models.domain.EvolutionChains;
import com.testmodyo.getpokemon.models.domain.Pokemon;
import com.testmodyo.getpokemon.models.domain.PokemonDescription;
import com.testmodyo.getpokemon.models.domain.PokemonSpecie;
import com.testmodyo.getpokemon.models.dto.PokemonDetailDto;
import com.testmodyo.getpokemon.models.dto.PokemonDto;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Juan David Gallego This is the main service for the pokemon query, it
 *         implements cache for the optimization of the requests
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class PokemonService implements IPokemonService {

	private final IMapperService iMapperService;

	private final ICallPokeApiService iCallPokeApiService;

	/**
	 * It obtains a pokemon from the cache, if it is not found, the POKEAPI service
	 * is queried and finally it is mapped to a DTO
	 * 
	 * @param id Identifier of the pokemon by which the search is carried ou
	 * @return PokemonDto Object mapped and returned in response
	 */
	@Override
	@Cacheable(value = CommonConstants.DEFAULT_CACHE_POKEMON)
	public PokemonDto getPokemon(int id) throws BusinessException {
		log.info("PokemonService getPokemon");
		return iMapperService.pokemonToPokemonDto(iCallPokeApiService.getPokemon(id));
	}

	/**
	 * It obtains a pokemon details from the cache, if it is not found, the POKEAPI
	 * service is queried, then additional information is consulted
	 * 
	 * @param id Identifier of the pokemon by which the search is carried ou
	 * @return PokemonDetailDto Object mapped and returned in response
	 */
	@Override
	@Cacheable(value = CommonConstants.DEFAULT_CACHE_POKEMON_DETAIL)
	public PokemonDetailDto getPokemonDetail(int id) throws BusinessException {
		log.info("PokemonService getPokemonDetail");
		Pokemon pokemon = iCallPokeApiService.getPokemon(id);
		return getAdditionalInfo(pokemon);

	}

	/**
	 * This method is in charge of obtaining the details of a pokemon, it obtains
	 * the description and the evolutions of the pokemon based on the species
	 * 
	 * @param Pokemon Contains the information of a pokemon
	 * @return PokemonDetailDto Object mapped and returned in response
	 */
	private PokemonDetailDto getAdditionalInfo(Pokemon pokemon) throws BusinessException {
		String description = getPokemonDescription(pokemon);
		PokemonSpecie pokemonSpecie = iCallPokeApiService.getPokemonSpecie(pokemon.getSpecies().getUrl());
		EvolutionChains evolutions = iCallPokeApiService.getEvolutions(pokemonSpecie.getEvolutionChain().getUrl());
		return iMapperService.pokemonDtoToPokemonDetailDto(iMapperService.pokemonToPokemonDto(pokemon),
				getPokemonsEvolutions(evolutions, pokemon.getId()), description);
	}

	/**
	 * It is responsible for obtaining the description of a pokemon, implementing
	 * the POKEAPI service call, if the description is not found, a default value is
	 * returned
	 * 
	 * @param Pokemon Contains the information of a pokemon
	 * @return String description of the pokemon
	 */
	private String getPokemonDescription(Pokemon pokemon) throws BusinessException {
		PokemonDescription pokemonDescription = iCallPokeApiService.getPokemonDescription(pokemon.getId());
		String description = CommonConstants.DEFAULT_NOT_DESCRIPTION;
		if (pokemonDescription != null && pokemonDescription.getDescriptions() != null
				&& pokemonDescription.getDescriptions().size() >= 2)
			description = pokemonDescription.getDescriptions().get(2).getDescription();
		return description;
	}

	/**
	 * 
	 * Is in charge of obtaining the evolutions of a pokemon, iterates over the
	 * evolution list and consults each pokemon
	 * 
	 * @param evolutions Contains the list pokemon evolutions
	 * @param id         Contains the id of a pokemon
	 * @return List<PokemonDto> List of evolutions of a pokemon
	 */
	private List<PokemonDto> getPokemonsEvolutions(EvolutionChains evolutions, int id) throws BusinessException {
		log.info("PokemonService getPokemonEvolutions");
		List<PokemonDto> pokemonsEvolutions = new LinkedList<>();
		List<Chain> chains = evolutions.getChain().getEvolvesTo();
		for (Chain chain : chains) {
			addPokemonEvolution(id, chain, pokemonsEvolutions);
			chains = chain.getEvolvesTo();
		}
		if (!chains.isEmpty() && chains.get(0) != null)
			addPokemonEvolution(id, chains.get(0), pokemonsEvolutions);
		return pokemonsEvolutions;
	}

	/**
	 * 
	 * In charge of adding a pokemon to the evolution list if it does not refer to
	 * the same origin pokemon
	 * 
	 * @param chain              Object containing the evolution species of a
	 *                           pokemon
	 * @param id                 Contains the id of a pokemo
	 * @param pokemonsEvolutions Contains the current list of evolutions
	 */
	private void addPokemonEvolution(int id, Chain chain, List<PokemonDto> pokemonsEvolutions)
			throws BusinessException {
		log.info("PokemonService addPokemonEvolutions");
		String[] aux = chain.getSpecies().getUrl().split("/");
		Integer index = Integer.valueOf(aux[aux.length - 1]);
		if (index != id) {
			pokemonsEvolutions.add(iMapperService.pokemonToPokemonDto(iCallPokeApiService.getPokemon(index)));
		}
	}

}

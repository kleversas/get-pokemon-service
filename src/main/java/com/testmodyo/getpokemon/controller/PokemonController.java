package com.testmodyo.getpokemon.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.testmodyo.getpokemon.common.CommonConstants;
import com.testmodyo.getpokemon.controller.exception.BusinessException;
import com.testmodyo.getpokemon.models.dto.PokemonDetailDto;
import com.testmodyo.getpokemon.models.dto.PokemonDto;
import com.testmodyo.getpokemon.models.dto.ResponseDto;
import com.testmodyo.getpokemon.service.IPokemonService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Juan David Gallego
 * 
 *         Controlling class of requests to the different services, returns a
 *         generic response for each one
 * 
 */
@RestController
@RequestMapping(value = "/api/v1/pokemon")
@CrossOrigin("*")
@Slf4j
@RequiredArgsConstructor
public class PokemonController {

	private final IPokemonService iPokemonService;

	/**
	 * Allows to receive requests for a pokemon query, converts the output into a
	 * user-readable DTO
	 * 
	 * @param id Identifier of the pokemon by which the search is carried out
	 * @return ResponseEntity<ResponseDto<PokemonDto>> Object returned in response
	 */
	@GetMapping(value = "/get-pokemon/{id}")
	@ApiOperation(value = "find pokemon", notes = "Get pokemon.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successful operation."),
			@ApiResponse(code = 404, message = "Not found resource"),
			@ApiResponse(code = 400, message = "Bad Request. The request is invalid"), })
	public ResponseEntity<ResponseDto<PokemonDto>> getPokemons(@PathVariable int id) throws BusinessException {
		log.info("get pokemon request");
		PokemonDto pokemon = iPokemonService.getPokemon(id);
		ResponseDto<PokemonDto> response = new ResponseDto<>(CommonConstants.SUCCESSFUL_OPERATION,
				CommonConstants.SUCCESSFUL_OPERATION, HttpStatus.OK.value(), pokemon);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	/**
	 * Allows to receive requests for a pokemon query, converts the output into a
	 * user-readable DTO
	 * 
	 * @param id Identifier of the pokemon by which the search is carried out
	 * @return ResponseEntity<ResponseDto<PokemonDto>> Object returned in response
	 */
	@GetMapping(value = "/get-pokemon-detail/{id}")
	@ApiOperation(value = "find pokemon detail", notes = "Get pokemon detail.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successful operation."),
			@ApiResponse(code = 404, message = "Not found resource"),
			@ApiResponse(code = 400, message = "Bad Request. The request is invalid"), })
	public ResponseEntity<ResponseDto<PokemonDetailDto>> getPokemonDetail(@PathVariable int id)
			throws BusinessException {
		log.info("get pokemon detail request");
		PokemonDetailDto pokemon = iPokemonService.getPokemonDetail(id);
		ResponseDto<PokemonDetailDto> response = new ResponseDto<>(CommonConstants.SUCCESSFUL_OPERATION,
				CommonConstants.SUCCESSFUL_OPERATION, HttpStatus.OK.value(), pokemon);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

}

package com.testmodyo.getpokemon.controller.exception;

import lombok.Data;

/**
 * @author Juan David Gallego
 * Class for handling exceptions within the application
 */
@Data
public class BusinessException extends Exception {

	private static final long serialVersionUID = 1L;

	private final String code;
	private final String description;
	private final int status;

	public BusinessException(String code, String description, int status) {
		super();
		this.code = code;
		this.description = description;
		this.status = status;
	}

}

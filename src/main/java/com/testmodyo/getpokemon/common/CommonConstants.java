package com.testmodyo.getpokemon.common;
 
/**
 * @author Juan David Gallego
 * This class contains a class of constants used for project development, default values, error messages and cache names
 */
public class CommonConstants {

	private CommonConstants() {
	}

	// DEFAULT
	public static final String DEFAULT_BASE_PACKAGE = "com.testmodyo.getpokemon.controller";
	public static final String DEFAULT_REQUEST = "Request: ";
	public static final String DEFAULT_NOT_DESCRIPTION = "There is no description";

	public static final String DEFAULT_RAISED = " raised ";

	// ERRORS
	public static final String SUCCESSFUL_OPERATION = "SUCCESSFUL_OPERATION";
	public static final String INVALID_VALUE = "INVALID_VALUE";
	public static final String RESOURCE_NOT_FOUND = "RESOURCE_NOT_FOUND";
	public static final String INVALID_REQUEST = "INVALID_REQUEST";
	public static final String APPLICATION_ERROR = "APPLICATION_ERROR";
	public static final String CLIENT_ERROR_GET_POKEMON = "CLIENT_ERROR_GET_POKEMON";
	public static final String CLIENT_ERROR_GET_SPECIES = "CLIENT_ERROR_GET_SPECIES";
	public static final String CLIENT_ERROR_GET_EVOLUTIONS = "CLIENT_ERROR_GET_EVOLUTIONS";
	public static final String CLIENT_ERROR_GET_DESCRIPTION = "CLIENT_ERROR_GET_DESCRIPTION";



	// MESSAGES
	public static final String INVALID_PAGE = "The page number exceeds the maximum allowed";
	public static final String INVALID_ID_POKEMON = "The number of pokemon does not exist";
	public static final String MESSAGE_NOT_FOUND_POKEMON = "Could not find pokemon";
	public static final String MESSAGE_NOT_FOUND_SPECIE_POKEMON = "Could not find specie pokemon";
	public static final String MESSAGE_NOT_FOUND_EVOLUTIONS_POKEMON = "Could not find evolutions pokemon";


	public static final String MESSAGE_NOT_FOUND_DESCRIPTION_POKEMON = "Could not find description pokemon with identifier {}";
	
	//CACHE
	public static final String DEFAULT_CACHE_POKEMON = "Pokemon";
	public static final String DEFAULT_CACHE_POKEMON_DETAIL = "Pokemondetail";



}

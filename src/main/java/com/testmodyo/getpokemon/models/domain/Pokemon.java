package com.testmodyo.getpokemon.models.domain;

import java.io.Serializable;
import java.util.List;

import lombok.Data;
import lombok.Generated;

@Generated
@Data
public class Pokemon implements Serializable {

	private static final long serialVersionUID = 1L;
	private Integer id;
	private String name;
	private Integer weight;
	private Specie species;
	private List<TypeDetail> types;
	private List<AbilityDetail> abilities;
	private Sprites sprites;

}

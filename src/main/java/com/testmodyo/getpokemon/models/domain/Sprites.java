package com.testmodyo.getpokemon.models.domain;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.Generated;

@Generated
@Data
public class Sprites implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonProperty("back_default")
	private String backDefault;

}

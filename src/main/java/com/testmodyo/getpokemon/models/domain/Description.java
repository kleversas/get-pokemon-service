package com.testmodyo.getpokemon.models.domain;

import java.io.Serializable;

import lombok.Data;
import lombok.Generated;

@Generated
@Data
public class Description implements Serializable {

	private static final long serialVersionUID = 1L;
	private String description;
}

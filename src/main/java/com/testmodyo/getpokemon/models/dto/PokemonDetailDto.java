package com.testmodyo.getpokemon.models.dto;

import java.io.Serializable;
import java.util.List;

import lombok.Builder;
import lombok.Data;
import lombok.Generated;

@Generated
@Data
@Builder
public class PokemonDetailDto implements Serializable {

	private static final long serialVersionUID = 1L;
	private PokemonDto pokemon;
	private String description;
	private List<PokemonDto> evolutions;

}

package com.testmodyo.getpokemon.config;

import java.util.Arrays;

import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCache;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import com.testmodyo.getpokemon.common.CommonConstants;

import lombok.Generated;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

/**
 * @author Juan David Gallego This class of configuration, contains swagger,
 *         restemplate, and cache instances settings
 */
@Generated
@Configuration
@EnableCaching
public class AppConfig {

	/**
	 * This method performs the swagger configuration for the apis documentation
	 * 
	 * @return Docket This returns the parameterized docket instance
	 */
	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.basePackage(CommonConstants.DEFAULT_BASE_PACKAGE))
				.paths(PathSelectors.any()).build();
	}

	/**
	 * This method generates a restemplate instance
	 * 
	 * @return Restemplate This returns the restemplate instance
	 */
	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

	/**
	 * 
	 * The method sets the cache lists
	 * 
	 * @return CacheManager This returns the cacheManager instance
	 */
	@Bean
	public CacheManager cacheManager() {
		SimpleCacheManager cacheManager = new SimpleCacheManager();
		cacheManager.setCaches(Arrays.asList(new ConcurrentMapCache(CommonConstants.DEFAULT_CACHE_POKEMON),
				new ConcurrentMapCache(CommonConstants.DEFAULT_CACHE_POKEMON_DETAIL)));
		return cacheManager;
	}

}

package com.testmodyo.getpokemon.external;

import com.testmodyo.getpokemon.controller.exception.BusinessException;
import com.testmodyo.getpokemon.models.domain.EvolutionChains;
import com.testmodyo.getpokemon.models.domain.Pokemon;
import com.testmodyo.getpokemon.models.domain.PokemonDescription;
import com.testmodyo.getpokemon.models.domain.PokemonSpecie;

public interface ICallPokeApiService {

	public Pokemon getPokemon(Integer id) throws BusinessException;

	public PokemonDescription getPokemonDescription(Integer id) throws BusinessException;

	public PokemonSpecie getPokemonSpecie(String url) throws BusinessException;

	public EvolutionChains getEvolutions(String url) throws BusinessException;

}

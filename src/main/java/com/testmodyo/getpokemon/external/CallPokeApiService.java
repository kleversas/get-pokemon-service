package com.testmodyo.getpokemon.external;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.testmodyo.getpokemon.common.CommonConstants;
import com.testmodyo.getpokemon.controller.exception.BusinessException;
import com.testmodyo.getpokemon.models.domain.EvolutionChains;
import com.testmodyo.getpokemon.models.domain.Pokemon;
import com.testmodyo.getpokemon.models.domain.PokemonDescription;
import com.testmodyo.getpokemon.models.domain.PokemonSpecie;
import com.testmodyo.getpokemon.utils.UtilCallService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Juan David Gallego
 * 
 *         In charge of making the requests and mapping the responses of the
 *         different requests
 */
@Service
@Slf4j
public class CallPokeApiService implements ICallPokeApiService {

	@Value("${spring.client.poke-api-get-pokemon}")
	private String urlGetPokemon;

	@Value("${spring.client.poke-api-get-characteristics}")
	private String urlGetPokemonDescription;

	@Value("${spring.client.poke-api-get-evolutions}")
	private String urlGetPokemonEvolutions;

	@Autowired
	RestTemplate restTemplate;

	/**
	 * Implement the POKEAPI getpokemon service call, build the url with its
	 * parameters and make the request
	 * 
	 * @param id      Contains the id of a pokemon
	 * @param Pokemon Contains the pokemon information consulted
	 */
	@Override
	public Pokemon getPokemon(Integer id) throws BusinessException {
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(urlGetPokemon.concat(id.toString()));
		log.info("Request to get pokemon with id {} sent.", id);
		ResponseEntity<Pokemon> response = UtilCallService.consumeService(restTemplate, builder.toUriString(),
				new HttpHeaders(), HttpMethod.GET);
		log.info("Request to get pokemon finalized.");
		return response.getBody();
	}

	/**
	 * Implement the POKEAPI getpokemonDetail service call, build the url with its
	 * parameters and make the request
	 * 
	 * @param id                 Contains the id of a pokemon
	 * @param PokemonDescription Contains the pokemon description in different
	 *                           languajes
	 */
	@Override
	public PokemonDescription getPokemonDescription(Integer id) throws BusinessException {
		try {
			UriComponentsBuilder builder = UriComponentsBuilder
					.fromHttpUrl(urlGetPokemonDescription.concat(id.toString()));
			log.info("Request to get description pokemon with id {} sent.", id);
			ResponseEntity<PokemonDescription> response = UtilCallService.consumeServiceDescription(restTemplate,
					builder.toUriString(), new HttpHeaders(), HttpMethod.GET);
			log.info("Request to get description pokemon finalized.");
			return response.getBody();

		} catch (BusinessException e) {
			log.warn(CommonConstants.MESSAGE_NOT_FOUND_DESCRIPTION_POKEMON);
			return null;
		}

	}

	/**
	 * Implement the POKEAPI getpokemonSpecie service call, build the url with its
	 * parameters and make the request
	 * 
	 * @param url           Contains the url for service call
	 * @param PokemonSpecie Contains the object evolutions chain
	 */
	@Override
	public PokemonSpecie getPokemonSpecie(String url) throws BusinessException {
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url);
		log.info("Request to get specie pokemon  sent.");
		ResponseEntity<PokemonSpecie> response = UtilCallService.consumeServiceSpecie(restTemplate,
				builder.toUriString(), new HttpHeaders(), HttpMethod.GET);
		log.info("Request to get specie pokemon finalized.");
		return response.getBody();

	}

	/**
	 * Implement the POKEAPI getpokemonEvolutions service call, build the url with
	 * its parameters and make the request
	 * 
	 * @param url             Contains the url for service call
	 * @param EvolutionChains Contains the object with the different species of
	 *                        evolutions of the pokemon
	 */
	@Override
	public EvolutionChains getEvolutions(String url) throws BusinessException {
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url);
		log.info("Request to get evolutions pokemon  sent.");
		ResponseEntity<EvolutionChains> response = UtilCallService.consumeServiceEvolutions(restTemplate,
				builder.toUriString(), new HttpHeaders(), HttpMethod.GET);
		log.info("Request to get evolutions pokemon finalized.");
		return response.getBody();

	}

}

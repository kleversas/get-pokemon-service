package com.testmodyo.getpokemon.utils;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.client.UnknownHttpStatusCodeException;

import com.testmodyo.getpokemon.common.CommonConstants;
import com.testmodyo.getpokemon.controller.exception.BusinessException;
import com.testmodyo.getpokemon.models.domain.EvolutionChains;
import com.testmodyo.getpokemon.models.domain.Pokemon;
import com.testmodyo.getpokemon.models.domain.PokemonDescription;
import com.testmodyo.getpokemon.models.domain.PokemonSpecie;

/**
 * @author Juan David Gallego Utility to make the call to POKEAPI
 */
public class UtilCallService {

	private UtilCallService() {

	}

	/**
	 * It is responsible for making the call to the client and generating an
	 * exception in case an error occurs
	 * 
	 * @param restTemplate Object for making the call to the POKEAPI client
	 * @param url          URL where the request is made
	 * @param headers      Headers containing the request
	 * @param method       Type of method to make the request
	 * @return ResponseEntity<Pokemon> containing the answer with the pokemon
	 *         information
	 */
	public static ResponseEntity<Pokemon> consumeService(RestTemplate restTemplate, String url, HttpHeaders headers,
			HttpMethod method) throws BusinessException {
		try {
			return restTemplate.exchange(url, method, new HttpEntity<>(headers), Pokemon.class);
		} catch (HttpClientErrorException | HttpServerErrorException | UnknownHttpStatusCodeException e) {
			throw new BusinessException(CommonConstants.CLIENT_ERROR_GET_POKEMON, e.getMessage(), e.getRawStatusCode());
		}
	}

	/**
	 * It is responsible for making the call to the client and generating an
	 * exception in case an error occurs
	 * 
	 * @param restTemplate Object for making the call to the POKEAPI client
	 * @param url          URL where the request is made
	 * @param headers      Headers containing the request
	 * @param method       Type of method to make the request
	 * @return ResponseEntity<PokemonDescription> containing the answer with the
	 *         pokemon description
	 */
	public static ResponseEntity<PokemonDescription> consumeServiceDescription(RestTemplate restTemplate, String url,
			HttpHeaders headers, HttpMethod method) throws BusinessException {
		try {
			return restTemplate.exchange(url, method, new HttpEntity<>(headers), PokemonDescription.class);
		} catch (HttpClientErrorException | HttpServerErrorException | UnknownHttpStatusCodeException e) {
			throw new BusinessException(CommonConstants.CLIENT_ERROR_GET_DESCRIPTION, e.getMessage(),
					e.getRawStatusCode());
		}
	}

	/**
	 * It is responsible for making the call to the client and generating an
	 * exception in case an error occurs
	 * 
	 * @param restTemplate Object for making the call to the POKEAPI client
	 * @param url          URL where the request is made
	 * @param headers      Headers containing the request
	 * @param method       Type of method to make the request
	 * @return ResponseEntity<EvolutionChains> containing the answer with the
	 *         pokemon evolutions
	 */
	public static ResponseEntity<EvolutionChains> consumeServiceEvolutions(RestTemplate restTemplate, String url,
			HttpHeaders headers, HttpMethod method) throws BusinessException {
		try {
			return restTemplate.exchange(url, method, new HttpEntity<>(headers), EvolutionChains.class);
		} catch (HttpClientErrorException | HttpServerErrorException | UnknownHttpStatusCodeException e) {
			throw new BusinessException(CommonConstants.CLIENT_ERROR_GET_EVOLUTIONS, e.getMessage(),
					e.getRawStatusCode());
		}
	}

	/**
	 * It is responsible for making the call to the client and generating an
	 * exception in case an error occurs
	 * 
	 * @param restTemplate Object for making the call to the POKEAPI client
	 * @param url          URL where the request is made
	 * @param headers      Headers containing the request
	 * @param method       Type of method to make the request
	 * @return ResponseEntity<PokemonSpecie> containing the answer with the pokemon
	 *         species
	 */
	public static ResponseEntity<PokemonSpecie> consumeServiceSpecie(RestTemplate restTemplate, String url,
			HttpHeaders headers, HttpMethod method) throws BusinessException {
		try {
			return restTemplate.exchange(url, method, new HttpEntity<>(headers), PokemonSpecie.class);
		} catch (HttpClientErrorException | HttpServerErrorException | UnknownHttpStatusCodeException e) {
			throw new BusinessException(CommonConstants.CLIENT_ERROR_GET_SPECIES, e.getMessage(), e.getRawStatusCode());
		}
	}

}
